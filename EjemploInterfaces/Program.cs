﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploInterfaces
{
    class Program
    {
        static void Main(string[] args)
        {

            Factura factura = new Factura();
            factura.numero = "123";

            ReciboSueldo reciboSueldo = new ReciboSueldo();
            reciboSueldo.legajo = "6666";

            FacturaDeLuz facturaLuz = new FacturaDeLuz();
            facturaLuz.cliente = "pepe argento";

            OtraCosa otra = new OtraCosa();

            List<IImprimible> objetosIMpribles = new List<IImprimible>();

            objetosIMpribles.Add(factura);
            objetosIMpribles.Add(reciboSueldo);
            objetosIMpribles.Add(facturaLuz);
            objetosIMpribles.Add(otra);


            List<IArchivable> objetosArchivables = new List<IArchivable>();
            objetosArchivables.Add(factura);
            objetosArchivables.Add(reciboSueldo);

            FacturaAbstracta fa = new Factura();

            


            foreach (var item in objetosIMpribles)
            {
                item.Imprimir();
            }

            foreach (var item in objetosArchivables)
            {
                item.Archivar();
            }

            var impresora = new Impresora();


            Console.ReadKey();
        }
    }



    public abstract class FacturaAbstracta  : IImprimible
    {
        public string numero { get; set; }

        public string VerNumero()
        {
            return numero;
        }

        public abstract void VerCodigoDeBarras();

        public void Imprimir()
        {
            Console.WriteLine("Imprimiendo factura " + numero);
        }
    }

    public interface IArchivable
    {
        void Archivar();
    }

    public interface IImprimible
    {
        void Imprimir();
    }

    public class Factura : FacturaAbstracta,  IArchivable
    {
       

        public void Archivar()
        {
            Console.WriteLine("Archivando factura " + numero);
        }

       

        public override void VerCodigoDeBarras()
        {
            Console.WriteLine("Codigo de barras   **" + numero+"**");
        }
    }

    public class ReciboSueldo : IImprimible, IArchivable
    {
        public string legajo { get; set; }

        public void Imprimir()
        {
            Console.WriteLine("REcibo Sueldo" + legajo);
        }

        public void Archivar()
        {
            Console.WriteLine("Archivando recibo Sueldo" + legajo);
        }
    }


    public class OtraCosa : IImprimible
       {
        public void Imprimir()
        {
            Console.WriteLine("impprimiendo otra cosa");
        }

       
        }

    public class FacturaDeLuz : FacturaAbstracta
    {
        public string cliente { get; set; }

       

        public override void VerCodigoDeBarras()
        {
            Console.WriteLine("Codigo de barras de la factura de luz de " + cliente);
        }
    }


    public class Impresora
    {


        public void Imprimir(IImprimible unObjetoCualquiera)
        {
            unObjetoCualquiera.Imprimir();
        }


       

    }



}
